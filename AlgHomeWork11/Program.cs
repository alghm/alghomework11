﻿using System.Diagnostics;
using System.Drawing;

internal class Program
{
    private static void Main(string[] args)
    {
        var tree1 = new SplayTree();
        var tree2 = new SplayTree();
        int N = 100000;
        var rnd = new Random();

        Console.WriteLine($"Tree1 - первое дерево - {N} чисел в случайном порядке");
        Console.WriteLine($"Tree2 - второе дерево - {N} чисел в возрастающем порядке");

        Console.WriteLine($"Тест для двух деревьев:");

        Stopwatch stopwatch = new Stopwatch();

        stopwatch.Start();
        for (int i = 0; i < N; i++)
        {
            tree1.Insert(rnd.Next(1, N));
        }
        stopwatch.Stop();
        long elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Вставка для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N; i++)
        {
            tree2.Insert(i);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Вставка для второго дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            var f = tree1.Search(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"Поиск {N / 10} элементов для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            var f = tree2.Search(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"Поиск {N / 10} элементов для второго дерева - {0}", elapsedTime);

        stopwatch.Restart();
        for (int i = 0; i < N; i++)
        {
            int rndNumber = rnd.Next(1, 11);
            var f = tree1.Search(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"Поиск {N} раз от 1 до 10 для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N; i++)
        {
            int rndNumber = rnd.Next(1, 11);
            var f = tree2.Search(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine($"Поиск {N} раз от 1 до 10 для второго дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            tree1.Remove(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Удаление для первого дерева - {0}", elapsedTime);


        stopwatch.Restart();
        for (int i = 0; i < N / 10; i++)
        {
            int rndNumber = rnd.Next(1, N);
            tree2.Remove(rndNumber);
        }
        stopwatch.Stop();
        elapsedTime = stopwatch.ElapsedMilliseconds;
        Console.WriteLine("Удаление для второго дерева - {0}", elapsedTime);

        Console.ReadLine();
    }
}

public class Node
{
    public int Value;
    public Node Left;
    public Node Right;
    public Node Parent;

    public Node(int value)
    {
        this.Value = value;
        this.Left = null;
        this.Right = null;
        this.Parent = null;
    }
}

public class RandomizedBSTree
{
    public class Node
    {
        public int Value { get; set; }
        public Node Left { get; set; }
        public Node Right { get; set; }
        public int Size { get; set; }
    }

    private Node root;

    public void Insert(int v)
    {
        if (root == null)
        {
            root = new Node { Value = v, Size = 1 };
        }
        else
        {
            if (new Random().Next(root.Size + 1) == 0)
            {
                root = InsertRoot(root, v);
            }
            else if (v < root.Value)
            {
                root.Left = Insert(root.Left, v);
            }
            else
            {
                root.Right = Insert(root.Right, v);
            }
        }
    }


    private Node Insert(Node p, int v)
    {
        if (p == null) return new Node { Value = v, Size = 1 };
        if (v < p.Value)
        {
            p.Left = Insert(p.Left, v);
        }
        else if (v > p.Value)
        {
            p.Right = Insert(p.Right, v);
        }
        p.Size = 1 + (p.Left?.Size ?? 0) + (p.Right?.Size ?? 0);
        return p;
    }

    private Node InsertRoot(Node p, int v)
    {
        if (p == null) return new Node { Value = v, Size = 1 };
        if (v < p.Value)
        {
            p.Left = InsertRoot(p.Left, v);
            p.Size = 1 + (p.Left?.Size ?? 0) + (p.Right?.Size ?? 0);
            return RotateRight(p);
        }
        else
        {
            p.Right = InsertRoot(p.Right, v);
            p.Size = 1 + (p.Left?.Size ?? 0) + (p.Right?.Size ?? 0);
            return RotateLeft(p);
        }
    }

    public bool Search(int v)
    {
        return Search(root, v);
    }

    private bool Search(Node p, int v)
    {
        if (p == null) return false;
        if (v < p.Value)
        {
            return Search(p.Left, v);
        }
        else if (v > p.Value)
        {
            return Search(p.Right, v);
        }
        else
        {
            return true;
        }
    }

    public void Remove(int k)
    {
        root = Remove(root, k);
    }

    private Node Remove(Node p, int k)
    {
        if (p == null) return p;
        if (k < p.Value)
        {
            p.Left = Remove(p.Left, k);
        }
        else if (k > p.Value)
        {
            p.Right = Remove(p.Right, k);
        }
        else
        {
            if (p.Left == null) return p.Right;
            else if (p.Right == null) return p.Left;
            else
            {
                p.Value = MinValue(p.Right);
                p.Right = Remove(p.Right, p.Value);
            }
        }
        p.Size = 1 + (p.Left?.Size ?? 0) + (p.Right?.Size ?? 0);
        return p;
    }

    private int MinValue(Node node)
    {
        int minv = node.Value;
        while (node.Left != null)
        {
            minv = node.Left.Value;
            node = node.Left;
        }
        return minv;
    }

    private Node RotateRight(Node p)
    {
        Node q = p.Left;
        if (q == null) return p;
        p.Left = q.Right;
        q.Right = p;
        return q;
    }

    private Node RotateLeft(Node q)
    {
        Node p = q.Right;
        if (p == null) return q;
        q.Right = p.Left;
        p.Left = q;
        return p;
    }
}

public class SplayTree
{
    public Node Root;

    public SplayTree()
    {
        Root = null;
    }

    public void Insert(int data)
    {
        Node newNode = new Node(data);
        if (Root == null)
        {
            Root = newNode;
        }
        else
        {
            Node current = Root;
            while (true)
            {
                if (data < current.Value)
                {
                    if (current.Left == null)
                    {
                        current.Left = newNode;
                        newNode.Parent = current;
                        break;
                    }
                    else
                    {
                        current = current.Left;
                    }
                }
                else
                {
                    if (current.Right == null)
                    {
                        current.Right = newNode;
                        newNode.Parent = current;
                        break;
                    }
                    else
                    {
                        current = current.Right;
                    }
                }
            }
        }
        Splay(newNode);
    }

    public Node Search(int data)
    {
        Node current = Root;
        while (current != null)
        {
            if (data < current.Value)
            {
                current = current.Left;
            }
            else if (data > current.Value)
            {
                current = current.Right;
            }
            else
            {
                Splay(current);
                return current;
            }
        }
        return null;
    }

    public void Remove(int data)
    {
        Node node = Search(data);
        if (node == null)
        {
            return;
        }

        Splay(node);

        if (node.Left == null)
        {
            Move(node, node.Right);
        }
        else if (node.Right == null)
        {
            Move(node, node.Left);
        }
        else
        {
            Node min = Minimum(node.Right);
            if (min.Parent != node)
            {
                Move(min, min.Right);
                min.Right = node.Right;
                min.Right.Parent = min;
            }
            Move(node, min);
            min.Left = node.Left;
            min.Left.Parent = min;
        }
    }

    private void Splay(Node node)
    {
        while (node != Root)
        {
            if (node.Parent == Root)
            {
                if (node == node.Parent.Left)
                {
                    Root = node.Parent;
                    RightRotate(node.Parent);
                }
                else
                {
                    Root = node.Parent;
                    LeftRotate(node.Parent);
                }
            }
            else if (node == node.Parent.Left && node.Parent == node.Parent.Parent.Left)
            {
                RightRotate(node.Parent.Parent);
                RightRotate(node.Parent);
            }
            else if (node == node.Parent.Right && node.Parent == node.Parent.Parent.Right)
            {
                LeftRotate(node.Parent.Parent);
                LeftRotate(node.Parent);
            }
            else if (node == node.Parent.Left && node.Parent == node.Parent.Parent.Right)
            {
                RightRotate(node.Parent);
                LeftRotate(node.Parent);
            }
            else if (node == node.Parent.Right && node.Parent == node.Parent.Parent.Left)
            {
                LeftRotate(node.Parent);
                RightRotate(node.Parent);
            }
            else
            {
                if (node == node.Parent.Right)
                {
                    LeftRotate(node.Parent);
                }
                else
                {
                    RightRotate(node.Parent);
                }
            }
        }
    }

    private void Move(Node u, Node v)
    {
        if (u.Parent == null)
        {
            Root = v;
        }
        else if (u == u.Parent.Left)
        {
            u.Parent.Left = v;
        }
        else
        {
            u.Parent.Right = v;
        }
        if (v != null)
        {
            v.Parent = u.Parent;
        }
    }

    private Node Minimum(Node node)
    {
        while (node.Left != null)
        {
            node = node.Left;
        }
        return node;
    }

    private void LeftRotate(Node x)
    {
        Node y = x.Right;
        x.Right = y.Left;
        if (y.Left != null)
        {
            y.Left.Parent = x;
        }
        y.Parent = x.Parent;
        if (x.Parent == null)
        {
            Root = y;
        }
        else if (x == x.Parent.Left)
        {
            x.Parent.Left = y;
        }
        else
        {
            x.Parent.Right = y;
        }
        y.Left = x;
        x.Parent = y;
    }

    private void RightRotate(Node x)
    {
        Node y = x.Left;
        x.Left = y.Right;
        if (y.Right != null)
        {
            y.Right.Parent = x;
        }
        y.Parent = x.Parent;
        if (x.Parent == null)
        {
            Root = y;
        }
        else if (x == x.Parent.Right)
        {
            x.Parent.Right = y;
        }
        else
        {
            x.Parent.Left = y;
        }
        y.Right = x;
        x.Parent = y;
    }
}